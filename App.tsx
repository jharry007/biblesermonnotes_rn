import 'react-native-gesture-handler'
import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import ScreenTabs from './src/navigation/ScreenTabs';
import { DatabaseProvider } from './src/database/DatabaseContext';
import { TestIds, BannerAd, BannerAdSize} from '@react-native-firebase/admob';
const App = () => {
  const Stack = createStackNavigator();
  const AppState = ({ children }: { children: JSX.Element | JSX.Element[] }) => {
    return (
      <DatabaseProvider>
        {children}
      </DatabaseProvider>
    )
  }

  return (
    <NavigationContainer>
      <AppState>
        <Stack.Navigator screenOptions={{ headerShown: false }}>
          <Stack.Screen name="Home" component={ScreenTabs} />
        </Stack.Navigator>
      </AppState>
      <BannerAd
unitId={'ca-app-pub-4646433692422477/3939976017'}
size={BannerAdSize.SMART_BANNER}
requestOptions={{
requestNonPersonalizedAdsOnly: true,}}
onAdLoaded={() => {
console.log('Advert loaded');}}
onAdFailedToLoad={(error) => {
console.error('Advert failed to load: ', error);}}
/>
    </NavigationContainer>
  )
}

export default App;

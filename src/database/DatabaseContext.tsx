import React, { useEffect, useState } from 'react'
import { createContext } from 'react';
import { Alert } from 'react-native';
import SQLite from 'react-native-sqlite-storage';
import { Crypto } from '../interfaces/Crypto';
import { User } from '../interfaces/User';
import { Sermon } from '../interfaces/Sermon';
import { SermonAgg } from '../interfaces/SermonAgg';
import Moment from 'moment';

type DataBaseContextProps = {
  cryptos: Crypto[];
  sermons: Sermon[];
  sermonsAgg: SermonAgg[];
  user: User;
  currentPrice: number;
  singleCrypto: Crypto;
  singleSermon: Sermon;
  currentTotal: number;
  initialInvs: number;
  getData: () => Promise<void>;
  getSermonData: () => Promise<void>;
  getSermonDataByMonth: () => Promise<void>;
  createUser: (name: string) => Promise<void>;
  getPrice: () => Promise<void>;
  getSingleCrypto: (id: number) => Promise<void>;
  getSingleSermon: (id: number) => Promise<void>;
  getSermonsDetailByMonth:(monthYear:string) => Promise<void>;
  createTable: () => Promise<void>;
  createSermonTable: () => Promise<void>;
  setData: (name: string, price: string, quantity: number) => Promise<void>;
  setSermonData: (Key_verses: string, Speaker_name: string, Question_not_understand: string,Liked_things: string,Key_points: string, Category:string) => Promise<void>;
  updateData: (id: number, name: string, price: string, quantity: number) => Promise<void>;
  updateSermonData: (id: number,Key_verses: string, Speaker_name: string, Question_not_understand: string,Liked_things: string,Key_points: string, Category:string) => Promise<void>;
  editUser: (id: number, name: string) => Promise<void>
  deleteData: (id: number) => Promise<void>;
  deleteSermonData: (id: number) => Promise<void>;
}

export const DatabaseContext = createContext({} as DataBaseContextProps);

export const DatabaseProvider = ({ children }: any) => {
  const [cryptos, setCryptos] = useState<Crypto[]>([]);
  const [sermons, setSermons] = useState<Sermon[]>([]);
  const [sermonsAgg, setSermonsAgg] = useState<SermonAgg[]>([]);
  const [currentPrice, setCurrentPrice] = useState<number>(0)
  const [singleCrypto, setSingleCrypto] = useState<Crypto>({
    Id: 0,
    name: '',
    price: '',
    quantity: 0,
  });
  const [singleSermon, setSingleSermon] = useState<Sermon>({
    Id: 0,
    Key_verses: '',
    Speaker_name: '',
    Question_not_understand: '',
    Liked_things:'',
    Key_points:'',
    Category:'',
    SermonDate:''
  });
  const [currentTotal, setCurrentTotal] = useState<number>(0); 
  const [initialInvs, setInitialInvs] = useState<number>(0)
  const [user, setUser] = useState<User>({
    Id: 0,
    Name: ''
  });

  useEffect(() => {
    //populateSermonTable();
    //dropSermonTable();
    createTable();
    createSermonTable();
    createUserTable();
    // getPrice();
    // getUser();
    // getData();
    getSermonData();
    getSermonDataByMonth();
  }, [])

  useEffect(() => {
    getPorfolioValue();
  }, [cryptos, currentPrice])

  const getPorfolioValue = () => {
    let current:number = 0;
    let inv:number = 0;
    if (cryptos.length <= 0) return;
    cryptos.forEach(c => {
      current += (currentPrice * c.quantity)
      inv += (parseFloat(c.price) * c.quantity)
    })
    setCurrentTotal(current);
    setInitialInvs(inv);
  }

  const db = SQLite.openDatabase({
    name: 'rnsqlite.db',
    location: 'default',
  },
    () => { },
    error => {
      console.log(error);
    }
  )

  interface Coin {
    dogecoin: Currency
  }

  interface Currency {
    usd?: number
  }



  const getPrice = async () => {
    const res = await fetch('https://api.coingecko.com/api/v3/simple/price?ids=dogecoin&vs_currencies=usd');
    const price: Coin = await res.json();
    console.log(price.dogecoin.usd);
    if (price.dogecoin.usd)
      setCurrentPrice(price.dogecoin.usd)
    else
      return;
  }

  const createUserTable = async () => {
    db.transaction(tx => {
      tx.executeSql(`
        CREATE TABLE IF NOT EXISTS
        Users
        (Id INTEGER PRIMARY KEY AUTOINCREMENT, Name TEXT)
      `)
    })
  }

  const createUser = async (name: string) => {
    try {
      db.transaction(tx => {
        tx.executeSql(`
          INSERT INTO Users (Name)
          VALUES (?)
        `, [name])
      })
      getUser();
    } catch (error) {
      console.log(error);
    }
  }

  const editUser = async (id: number, name: string) => {
    try {
      db.transaction(tx => {
        tx.executeSql(`
          UPDATE Users 
          SET Name = '${name}'
          WHERE Id = '${id}'
        `)
      })
      getUser();
    } catch (error) {
      console.log(error);
    }
  }

  const createTable = async () => {
    db.transaction((tx) => {
      tx.executeSql(`
        CREATE TABLE IF NOT EXISTS 
        Cryptos
        (Id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, price VARCHAR(255), quantity DOUBLE);
      `)
    })
  }

  const createSermonTable = async () => {
    db.transaction((tx) => {
      tx.executeSql(`
        CREATE TABLE IF NOT EXISTS 
        Sermons
        (Id INTEGER PRIMARY KEY AUTOINCREMENT, Key_verses VARCHAR(255), Speaker_name VARCHAR(255), Question_not_understand TEXT, Liked_things TEXT,Key_points TEXT, Category TEXT, SermonDate TEXT);
      `)
    })
  }

  const dropTable = async () => {
    db.transaction(tx => {
      tx.executeSql(`
        DROP TABLE Users;
      `)
    })
  }

  const dropSermonTable = async () => {
    db.transaction(tx => {
      tx.executeSql(`
        DROP TABLE Sermons;
      `)
    })
  }

  const populateSermonTable = async () => {
    console.log('populatetable');
    db.transaction(tx => {
      tx.executeSql(`
      INSERT INTO 'Sermons' ('Key_verses', 'Speaker_name', 'Question_not_understand', 'Liked_things', 'Key_points', 'Category', 'SermonDate' ) VALUES
      ('Keyverses1', 'Speakername1', 'NotUnderstand1', 'Likedthings1', 'Keypoints1', 'Category1', 'Thu JAN 1 2021 12:34:06 GMT+0500 (PKT)'),
      ('Keyverses1', 'Speakername1', 'NotUnderstand1', 'Likedthings1', 'Keypoints1', 'Category1', 'Thu JAN 1 2021 12:34:06 GMT+0500 (PKT)'),
      ('Keyverses1', 'Speakername1', 'NotUnderstand1', 'Likedthings1', 'Keypoints1', 'Category1', 'Thu JAN 1 2021 12:34:06 GMT+0500 (PKT)'),
      ('Keyverses2', 'Speakername2', 'NotUnderstand2', 'Likedthings2', 'Keypoints2', 'Category2', 'Thu FEB 11 2021 12:34:06 GMT+0500 (PKT)'),
      ('Keyverses3', 'Speakername3', 'NotUnderstand3', 'Likedthings3', 'Keypoints3', 'Category3', 'Thu MAR 11 2021 12:34:06 GMT+0500 (PKT)'),
      ('Keyverses4', 'Speakername4', 'NotUnderstand4', 'Likedthings4', 'Keypoints4', 'Category4', 'Thu APR 11 2021 12:34:06 GMT+0500 (PKT)'),
      ('Keyverses4', 'Speakername4', 'NotUnderstand4', 'Likedthings4', 'Keypoints4', 'Category5', 'Thu MAY 11 2021 12:34:06 GMT+0500 (PKT)'),
      ('Keyverses4', 'Speakername4', 'NotUnderstand4', 'Likedthings4', 'Keypoints4', 'Category4', 'Thu JUN 11 2021 12:34:06 GMT+0500 (PKT)'),
      ('Keyverses4', 'Speakername4', 'NotUnderstand4', 'Likedthings4', 'Keypoints4', 'Category4', 'Thu JUL 11 2021 12:34:06 GMT+0500 (PKT)'),
      ('Keyverses4', 'Speakername4', 'NotUnderstand4', 'Likedthings4', 'Keypoints4', 'Category4', 'Thu AUG 11 2021 12:34:06 GMT+0500 (PKT)'),
      ('Keyverses4', 'Speakername4', 'NotUnderstand4', 'Likedthings4', 'Keypoints4', 'Category4', 'Thu SEP 11 2021 12:34:06 GMT+0500 (PKT)'),
      ('Keyverses4', 'Speakername4', 'NotUnderstand4', 'Likedthings4', 'Keypoints4', 'Category4', 'Thu OCT 11 2021 12:34:06 GMT+0500 (PKT)'),
      ('Keyverses4', 'Speakername4', 'NotUnderstand4', 'Likedthings4', 'Keypoints4', 'Category4', 'Thu NOV 11 2021 12:34:06 GMT+0500 (PKT)'),
      ('Keyverses4', 'Speakername4', 'NotUnderstand4', 'Likedthings4', 'Keypoints4', 'Category4', 'Thu NOV 11 2021 12:34:06 GMT+0500 (PKT)'),
      ('Keyverses4', 'Speakername4', 'NotUnderstand4', 'Likedthings4', 'Keypoints4', 'Category4', 'Thu NOV 11 2021 12:34:06 GMT+0500 (PKT)'),
      ('Keyverses4', 'Speakername4', 'NotUnderstand4', 'Likedthings4', 'Keypoints4', 'Category4', 'Thu NOV 11 2021 12:34:06 GMT+0500 (PKT)'),
      ('Keyverses12', 'Speakername12', 'NotUnderstand12', 'Likedthings12', 'Keypoints12', 'Category12', 'Thu DEC 11 2021 12:34:06 GMT+0500 (PKT)');
      `)
    })
    // db.transaction((tx)=>{
    //   tx.executeSql(        
    //     'INSERT INTO Sermons (Key_verses, Speaker_name, Question_not_understand, Liked_things, Key_points, Category, SermonDate) VALUES (?,?,?,?,?,?,?),(?,?,?,?,?,?,?),(?,?,?,?,?,?,?),(?,?,?,?,?,?,?)',
    //     ['Sylvester Stallone',2,4,'Elvis Presley',2,4,'Leslie Nelson',3,4],
    //     (tx, results) => {               
    //       if (results.rowsAffected > 0 ) {
    //         console.log('Insert success');              
    //       } else {
    //         console.log('Insert failed');
    //       }
    //     }
    //   );
    // });
  }

  const getUser = async () => {
    try {
      db.transaction(tx => {
        tx.executeSql(`
          SELECT * FROM Users
        `, [], (tx, res) => {
          for (let i = 0; i < res.rows.length; i++) {
            let user = res.rows.item(i);
            setUser(user);
          }
        })
      })
    } catch (e) {
      console.log(e);
    }
  }

  const getData = async () => {
    let dbCryptos: Crypto[] = [];
    try {
      db.transaction((tx) => {
        tx.executeSql(
          `SELECT * FROM Cryptos`,
          [], (tx, res) => {
            for (let i = 0; i < res.rows.length; i++) {
              console.log("Inside loop --------->");
              console.log("Item", res.rows.item(i));
              let crypto = res.rows.item(i)
              dbCryptos.push(crypto)
            }
            setCryptos(dbCryptos);
          })
      });
    } catch (error) {
      Alert.alert("ERROR", "Could not retrieve any users")
    }
  }

  const getSermonData = async () => {
    let dbSermons: Sermon[] = [];
    try {
      db.transaction((tx) => {
        tx.executeSql(
          `SELECT * FROM Sermons`,
          [], (tx, res) => {
            for (let i = 0; i < res.rows.length; i++) {
              console.log("Inside loop --------->");
              console.log("Item", res.rows.item(i));
              let sermon = res.rows.item(i)
              dbSermons.push(sermon)
            }
            setSermons(dbSermons);
          })
      });
    } catch (error) {
      Alert.alert("ERROR", "Could not retrieve any sermon")
    }
  }

  const getSermonDataByMonth = async () => {
    let dbSermons: SermonAgg[] = [];
    try {
      db.transaction((tx) => {
        tx.executeSql(
          `SELECT substr(SermonDate, 5, 3)|| '-' ||substr(SermonDate,12,4) as 'month',count(*) as sermonCount FROM Sermons Group By  substr(SermonDate, 5, 3),substr(SermonDate,12,4) order by Id asc`,
          [], (tx, res) => {
            for (let i = 0; i < res.rows.length; i++) {
              console.log("Group by loop--------->");
              console.log("Item", res.rows.item(i));
              let sermon = res.rows.item(i)
              dbSermons.push(sermon)
            }
            setSermonsAgg(dbSermons);
          })
      });
    } catch (error) {
      Alert.alert("ERROR", "Could not retrieve any sermon")
    }
  }

  const getSingleCrypto = async (id: number) => {
    try {
      db.transaction(async tx => {
        tx.executeSql(`
          SELECT * FROM Cryptos WHERE Id = ${id}
        `, [], async (tx, res) => {
          console.log("Inside getSingleCrypto", res.rows.item(0));
          const response: Crypto = res.rows.item(0);
          setSingleCrypto(response);
          return response;
        })
      })
    } catch (error) {

    }
  }

  const getSingleSermon = async (id: number) => {
    try {
      db.transaction(async tx => {
        tx.executeSql(`
          SELECT * FROM Sermons WHERE Id = ${id}
        `, [], async (tx, res) => {
          console.log("Inside getSingleSermon", res.rows.item(0));
          const response: Sermon = res.rows.item(0);
          setSingleSermon(response);
          return response;
        })
      })
    } catch (error) {

    }
  }

  const getSermonsDetailByMonth = async (monthYear: string) => {
    let dbSermons: Sermon[] = [];
    
    try {
      const queryWord = monthYear.split("-");
     // console.log(`SELECT * FROM Sermons WHERE  substr(SermonDate, 5, 3)= '${queryWord[0]}' and substr(SermonDate,12,4)= '${queryWord[1]}'`);
      db.transaction(async tx => {
        tx.executeSql(`
          SELECT * FROM Sermons WHERE  substr(SermonDate, 5, 3)= '${queryWord[0]}' and substr(SermonDate,12,4)= '${queryWord[1]}'
        `,  [], (tx, res) => {
          for (let i = 0; i < res.rows.length; i++) {
            console.log("Inside Sermons Detail--------->");
            console.log("Item", res.rows.item(i));
            let sermon = res.rows.item(i)
            dbSermons.push(sermon)
          }
          setSermons(dbSermons);
        })
    });
  } catch (error) {
    Alert.alert("ERROR", "Could not retrieve any sermon")
  }
}
  const setData = async (name: string, price: string, quantity: number) => {
    if (name.length == 0 || price.toString().length == 0 || quantity.toString().length == 0) {
      Alert.alert('Warnign!', 'Please make sure to have data!')
    }
    try {
      await db.transaction(async (tx) => {
        await tx.executeSql(
          `INSERT INTO Cryptos (name, price, quantity) VALUES (?, ?, ?)`, [name, price, quantity])
      })
      // Alert.alert("Crypto Purchase was created");
      getData();
    } catch (error) {
      //Alert.alert("Error", error);
    }
  }

  const setSermonData = async (Key_verses: string, Speaker_name: string, Question_not_understand: string,Liked_things: string,Key_points: string, Category:string) => {
    if (Key_verses.length == 0 || Speaker_name.length == 0 || Question_not_understand.length == 0 || Liked_things.length == 0) {
      Alert.alert('Warnign!', 'Please make sure to have data!')
    }
    try {
      await db.transaction(async (tx) => {
        await tx.executeSql(
          `INSERT INTO Sermons (Key_verses, Speaker_name, Question_not_understand, Liked_things, Key_points, Category, SermonDate) VALUES (?, ?, ?, ?, ?, ?, ?)`, [Key_verses, Speaker_name, Question_not_understand, Liked_things, Key_points, Category,Date()])
      })
      // Alert.alert("Crypto Purchase was created");
      //getSermonData();
      getSermonDataByMonth();
    } catch (error) {
      //Alert.alert("Error", error);
    }
  }

  const updateData = async (id: number, name: string, price: string, quantity: number) => {
    try {
      await db.transaction(async tx => {
        await tx.executeSql(`
          UPDATE Cryptos 
          SET name = '${name}', price = '${price}', quantity = ${quantity}
          WHERE Id = ${id}
        `)
      })
      getData();
    } catch (error) {

    }
  }

  const updateSermonData = async (id: number, Key_verses: string, Speaker_name: string, Question_not_understand: string,Liked_things: string,Key_points: string,Category:string) => {
    try {
      await db.transaction(async tx => {
        await tx.executeSql(`
          UPDATE Sermons 
          SET Key_verses = '${Key_verses}', Speaker_name = '${Speaker_name}', Question_not_understand = '${Question_not_understand}', Liked_things = '${Liked_things}', Key_points = '${Key_points}', Category = '${Category}'
          WHERE Id = ${id}
        `)
      })
      //getSermonData();
      getSermonDataByMonth();
    } catch (error) {

    }
  }

  const deleteData = async (id: number) => {
    try {
      await db.transaction(async tx => {
        await tx.executeSql(`
          DELETE FROM Cryptos where Id = ${id}; 
        `)
      })
      getData();
    } catch (error) {
      Alert.alert("The deletion wasn't able to be done");
    }
  }

  const deleteSermonData = async (id: number) => {
    try {
      await db.transaction(async tx => {
        await tx.executeSql(`
          DELETE FROM Sermons where Id = ${id}; 
        `)
      })
      getSermonData();
    } catch (error) {
      Alert.alert("The deletion wasn't able to be done");
    }
  }

  return (
    <DatabaseContext.Provider value={{
      cryptos,
      sermons,
      sermonsAgg,
      currentPrice,
      createUser,
      user,
      singleCrypto,
      singleSermon,
      createTable,
      createSermonTable,
      getData,
      getSermonData,
      getSermonDataByMonth,
      getSermonsDetailByMonth,
      currentTotal,
      initialInvs,
      getPrice,
      getSingleCrypto,
      getSingleSermon,
      setData,
      setSermonData,
      updateData,
      updateSermonData,
      editUser,
      deleteData,
      deleteSermonData
    }}>
      {children}
    </DatabaseContext.Provider>
  )

}

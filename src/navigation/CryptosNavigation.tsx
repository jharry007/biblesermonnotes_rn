import { createStackNavigator } from '@react-navigation/stack'
import React from 'react'
import EditScreen from '../screens/EditScreen';
import EditSermonScreen from '../screens/EditSermonScreen';
import HomeScreen from '../screens/HomeScreen';
import SermonScreen from '../screens/SermonScreen';
import ViewSermonScreen from '../screens/ViewSermonScreen';

export type CryptoStackParams = {
  HomeScreen: undefined;
  EditScreen: { routedId?: number, routedName?: string }
  EditSermonScreen: { routedId?: number, routedName?: string }
  ViewSermonScreen: { routedId?: number, routedName?: string }
  SermonScreen: { routedId?: string, routedName?: string }
}

const Stack = createStackNavigator<CryptoStackParams>();

const CryptosNavigation = (stack: CryptoStackParams) => {
  return (
    <Stack.Navigator
      screenOptions={{
        cardStyle: {
          backgroundColor: 'transparent'
        },
        headerStyle: {
          elevation: 0,
          shadowColor: 'transparent'
        }
      }}
    >
      <Stack.Screen
        name="HomeScreen"
        component={HomeScreen}
        options={{ title: 'Sermons Notes', headerTitleAlign: 'center' }}
      />
      <Stack.Screen
        name="EditScreen"
        component={EditScreen}
      options={{title: 'Edit Crypto', headerTitleAlign: 'center'}}
      />
        <Stack.Screen
        name="EditSermonScreen"
        component={EditSermonScreen}
      options={{title: 'Edit Sermon', headerTitleAlign: 'center'}}
      />
       <Stack.Screen
        name="ViewSermonScreen"
        component={ViewSermonScreen}
      options={{title: 'View Sermon', headerTitleAlign: 'center'}}
      />
       <Stack.Screen
        name="SermonScreen"
        component={SermonScreen}
      options={{title: 'Sermons', headerTitleAlign: 'center'}}
      />
      
    </Stack.Navigator>
  )
}

export default CryptosNavigation

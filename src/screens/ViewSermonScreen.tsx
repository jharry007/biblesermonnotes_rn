import { StackScreenProps } from '@react-navigation/stack'
import React, { useContext, useEffect, useState } from 'react'
import { Alert, KeyboardAvoidingView, Platform, ScrollView, StyleSheet, Text, TextInput } from 'react-native'
import { View } from 'react-native'
import CustomButton from '../components/CustomButton'
import { DatabaseContext } from '../database/DatabaseContext'
import { useForm } from '../hooks/useForm'
import { CryptoStackParams } from '../navigation/CryptosNavigation'
import { COLORS } from '../styles/Constants'
import { globalStyles } from '../styles/globalStyles'
// Import HTML to PDF
import RNHTMLtoPDF from 'react-native-html-to-pdf';
import Pdf from 'react-native-pdf';
//const RNHTMLtoPDF = require('react-native-html-to-pdf');
interface Props extends StackScreenProps<CryptoStackParams, 'ViewSermonScreen'> { };

const ViewSermonScreen = ({ navigation, route }: Props) => {
  const { getSingleSermon, singleSermon, updateSermonData, deleteSermonData } = useContext(DatabaseContext);
  const [loading, setLoading] = useState<boolean>(false);
  const { routedId = 0, routedName } = route.params;
  const { Id, Key_verses, Speaker_name, Question_not_understand, Liked_things, Key_points, Category,  onChange, setFormValue } = useForm({
    Id: routedId,
    Key_verses: '',
    Speaker_name: '',
    Question_not_understand: '',
    Liked_things:'',
    Key_points:'',
    Category:''
  });

  const [filePath, setFilePath] = useState('');

  useEffect(() => {
    loadSermon();
  }, [routedId])

  useEffect(() => {
    setValues()
  }, [singleSermon])

  

  const loadSermon = async () => {
    setLoading(true);
    if (routedId === 0) {
      Alert.alert("Id is null")
      return;
    }

    getSingleSermon(Id);
    setLoading(false);
  }

  const createPDF_File = async () => {
    let options = {
      // HTML Content for PDF.
      // I am putting all the HTML code in Single line but if you want to use large HTML code then you can use + Symbol to add them.
      html:
        '<h1 style="text-align: center;"><strong>'+Key_verses+'</strong></h1><p style="text-align: center;">'+Question_not_understand+'</p><p style="text-align: center;">'+Liked_things+'</p><p style="text-align: center;">'+Key_points+'</p><p style="text-align: center;"><strong>'+Speaker_name+'</strong></p>',
      // Setting UP File Name for PDF File.
      fileName: 'test',
      base64: true,
 
      //File directory in which the PDF File Will Store.
      directory:  (Platform.OS === 'ios')? 'Documents': 'Downloads',
    };
 
    let file = await RNHTMLtoPDF.convert(options);
 
    console.log(file.filePath);
    //setFilePath(file.filePath);

 
    await Alert.alert(
        ("'PDF Creation Confirmation' The pdf was created at this location: " +file.filePath)
        );
 
    // this.setState({filePath:file.filePath});
  }

  const setValues = async () => {
    setFormValue({
      Id: routedId,
      Key_verses: singleSermon.Key_verses,
    Speaker_name: singleSermon.Speaker_name,
    Question_not_understand: singleSermon.Question_not_understand,
    Liked_things:singleSermon.Liked_things,
    Key_points:singleSermon.Key_points,
    Category:singleSermon.Category
    });
  }

  const editSermon = () => {
    updateSermonData(Id, Key_verses, Speaker_name, Question_not_understand, Liked_things, Key_points, Category);
    navigation.navigate('HomeScreen');
    // navigation.navigate('EditSermonScreen', {
    //   routedId: item.Id,
      
    // })
  }
  const source = { uri: 'file:///Users/codlerslaptop/Library/Developer/CoreSimulator/Devices/2F4C681A-83B7-48E8-A5B7-F45DB9997BAE/data/Containers/Data/Application/44EC1E8D-B8D1-461E-BA3E-EC843955CD0D/Documents/test.pdf', cache: false };
  return (
    <KeyboardAvoidingView
    behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
    style={{flex: 1}}
    enabled={true}>
    <ScrollView>
    <View style={globalStyles.viewContainer}>
      <Text style={globalStyles.title}>
        Edit Sermon
      </Text>
      {!loading ?
        (
          <View style={globalStyles.inputBox}>
            <Text style={globalStyles.inputLabel}>Key Verses</Text>
            <Text style={globalStyles.input}>{Key_verses}</Text>
            <Text style={globalStyles.inputLabel}>Speaker Name</Text>
            <Text style={globalStyles.input}>{Speaker_name}</Text>
            <Text style={globalStyles.inputLabel}>Category</Text>
            <Text style={globalStyles.input}>{Category}</Text>
            <Text style={globalStyles.inputLabel}>Ask questions about things didn’t understand</Text>
               <View style={globalStyles.textAreaContainer} >
             
               <Text style={globalStyles.textArea}>{Question_not_understand}</Text>
                </View>
              <Text style={globalStyles.inputLabel}>Thing Liked During The Service</Text>
              <View style={globalStyles.textAreaContainer} >
             
              <Text style={globalStyles.textArea}>{Liked_things}</Text>
              </View>
              <Text style={globalStyles.inputLabel}>Key Points</Text>
              <View style={globalStyles.textAreaContainer} >
             
              <Text style={globalStyles.textArea}>{Key_points}</Text>
              </View>
            <CustomButton top={20} title="Export PDF Sermon" func={() => createPDF_File()} />
          </View>
        ) : (
          <Text>Loading ...</Text>
        )
      }

    </View>
    
    {/* <Pdf source={'/Users/codlerslaptop/Library/Developer/CoreSimulator/Devices/2F4C681A-83B7-48E8-A5B7-F45DB9997BAE/data/Containers/Data/Application/44EC1E8D-B8D1-461E-BA3E-EC843955CD0D/Documents/test.pdf'} /> */}
    {/* <PdfView page={1} source={''} /> */}
    </ScrollView>
    {/* <View style={globalStyles.container}>
                <Pdf
                    source={source}
                    onLoadComplete={(numberOfPages,filePath) => {
                        console.log(`Number of pages: ${numberOfPages}`);
                    }}
                    onPageChanged={(page,numberOfPages) => {
                        console.log(`Current page: ${page}`);
                    }}
                    onError={(error) => {
                        console.log(error);
                    }}
                    onPressLink={(uri) => {
                        console.log(`Link pressed: ${uri}`);
                    }}
                    style={globalStyles.pdf}/>
            </View> */}
    </KeyboardAvoidingView>
    
  )
}

export default ViewSermonScreen;


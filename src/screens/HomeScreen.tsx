import { StackScreenProps } from '@react-navigation/stack';
import React, { useContext, useEffect, useState } from 'react'
import { FlatList, Image, RefreshControl, ScrollView } from 'react-native';
import { Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import SplashScreen from 'react-native-splash-screen';
import CardTile from '../components/CardTile';
import { DatabaseContext } from '../database/DatabaseContext';
import { globalStyles } from '../styles/globalStyles';
import { homeScreenStyle } from '../styles/homeStyles';
import Icon from 'react-native-vector-icons/MaterialIcons';

interface Props extends StackScreenProps<any> { };

const HomeScreen = ({ navigation }: Props) => {
  const { sermonsAgg, getSermonDataByMonth } = useContext(DatabaseContext);
  const [isRefreshing, setIsRefreshing] = useState<boolean>(false)

  useEffect(() => {
    SplashScreen.hide();
    //getSermonDataByMonth();
  }, [])


  const refreshData = async () => {
    setIsRefreshing(true)
    await getSermonDataByMonth();
    //await getPrice();
    setIsRefreshing(false)
  }

  const StockList = () => {
    return (
      <View style={homeScreenStyle.listBox}>
        {sermonsAgg.length === 0 ? (
          <>
          <Text style={[globalStyles.title, {color: 'grey', marginTop: 40}]}>
            You Haven't Added Any Sermon
          </Text>
          <Text style={{color: 'grey', fontSize: 30}}>Press The Add Button To Begin</Text>
          </>
        ) : (
          <FlatList
            showsVerticalScrollIndicator={false}
            numColumns={3}
            data={sermonsAgg}
            keyExtractor={(c) => String(c.month)}
            renderItem={({ item }) => (
              <TouchableOpacity
                onPress={() => navigation.navigate('SermonScreen', {
                  routedId: item.month,
                  routeKey_verses: item.sermonCount,
                })}
              >
            <View
            style={{
              flex: 1,
              flexDirection: 'column',
              margin:20,
              width:'95%',
              flexWrap:'wrap'
            }}>
              <Text style={{ fontWeight:'bold',fontSize:19,color:'red'}}>
          {item.sermonCount}
          <Icon name="event-note" size={50} color={'black'} />
        </Text>
              <Text style={{ fontWeight:'bold',color:'green'}}>
          {item.month}
        </Text>
        
            {/* <Image
              style={homeScreenStyle.imageThumbnail}
              source={require('../../assets/test2.png')}
            /> */}
          </View>
                {/* <CardTile key={item.Id} name={item.name} price={item.price} current={currentPrice * item.quantity} quantity={item.quantity} /> */}
                {/* <CardTile key={item.month} name={item.month} price={item.month} current={item.sermonCount} quantity={item.sermonCount} /> */}
              </TouchableOpacity>
            )}
            refreshControl={
              <RefreshControl
                refreshing={isRefreshing}
                onRefresh={refreshData}
              />
            }
          />
        )}
      </View>
    )
  }

  return (
    <View style={globalStyles.viewContainer}>
      {/* Header */}
      {/* <View>
        <Text style={globalStyles.title}>
          Sermons Notes
        </Text>
      </View> */}
      {/* <PriceTicker /> */}
      {/* <PortfolioHeader /> */}
      <StockList />
    </View>
  )
}

export default HomeScreen

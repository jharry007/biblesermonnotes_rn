import { StackScreenProps } from '@react-navigation/stack'
import React, { useContext, useEffect, useState } from 'react'
import { Alert, KeyboardAvoidingView, Platform, ScrollView, StyleSheet, Text, TextInput } from 'react-native'
import { View } from 'react-native'
import CustomButton from '../components/CustomButton'
import { DatabaseContext } from '../database/DatabaseContext'
import { useForm } from '../hooks/useForm'
import { CryptoStackParams } from '../navigation/CryptosNavigation'
import { COLORS } from '../styles/Constants'
import { globalStyles } from '../styles/globalStyles'

interface Props extends StackScreenProps<CryptoStackParams, 'EditScreen'> { };

const EditSermonScreen = ({ navigation, route }: Props) => {
  const { getSingleSermon, singleSermon, updateSermonData, deleteSermonData } = useContext(DatabaseContext);
  const [loading, setLoading] = useState<boolean>(false);
  const { routedId = 0, routedName } = route.params;
  const { Id, Key_verses, Speaker_name, Question_not_understand, Liked_things, Key_points, Category,  onChange, setFormValue } = useForm({
    Id: routedId,
    Key_verses: '',
    Speaker_name: '',
    Question_not_understand: '',
    Liked_things:'',
    Key_points:'',
    Category:''
  });

  useEffect(() => {
    loadSermon();
  }, [routedId])

  useEffect(() => {
    setValues()
  }, [singleSermon])

  const loadSermon = async () => {
    setLoading(true);
    if (routedId === 0) {
      Alert.alert("Id is null")
      return;
    }

    getSingleSermon(Id);
    setLoading(false);
  }

  const setValues = async () => {
    setFormValue({
      Id: routedId,
      Key_verses: singleSermon.Key_verses,
    Speaker_name: singleSermon.Speaker_name,
    Question_not_understand: singleSermon.Question_not_understand,
    Liked_things:singleSermon.Liked_things,
    Key_points:singleSermon.Key_points,
    Category:singleSermon.Category
    });
  }

  const editSermon = () => {
    updateSermonData(Id, Key_verses, Speaker_name, Question_not_understand, Liked_things, Key_points, Category);
    navigation.navigate('HomeScreen');
    // navigation.navigate('EditSermonScreen', {
    //   routedId: item.Id,
      
    // })
  }

  const deleteSermon = () => {
    deleteSermonData(routedId);
    navigation.navigate('HomeScreen')
  }

  return (
    <KeyboardAvoidingView
    behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
    style={{flex: 1}}
    enabled={true}>
    <ScrollView>
    <View style={globalStyles.viewContainer}>
      <Text style={globalStyles.title}>
        Edit Sermon
      </Text>
      {!loading ?
        (
          <View style={globalStyles.inputBox}>
            <Text style={globalStyles.inputLabel}>Key Verses</Text>
            <TextInput
              onChangeText={value => onChange(value, 'Key_verses')}
              returnKeyType='done'
              value={Key_verses}
              style={globalStyles.input}
              placeholder="Sermon Key Verses" />
            <Text style={globalStyles.inputLabel}>Speaker Name</Text>
            <TextInput
              onChangeText={value => onChange(value, 'Speaker_name')}
              value={Speaker_name}
              style={globalStyles.input}
              returnKeyType='done'
            //   keyboardType={Platform.OS == 'android' ? "number-pad" : "numeric" }
              placeholder="Enter Speaker Name" />
            <Text style={globalStyles.inputLabel}>Category</Text>
            <TextInput
              onChangeText={value => onChange(value, 'Category')}
              value={Category}
              style={globalStyles.input}
              returnKeyType='done'
            //   keyboardType={Platform.OS == 'android' ? "number-pad" : "numeric" }
              placeholder="Enter Category" />
               <Text style={globalStyles.inputLabel}>Ask questions about things didn’t understand</Text>
               <View style={globalStyles.textAreaContainer} >
             
            <TextInput 
              style={globalStyles.textArea}
              underlineColorAndroid="transparent"
              placeholder="Enter Things You Don't Understand"
              placeholderTextColor="gray"
              numberOfLines={10}
              multiline={true}
              onChangeText={value => onChange(value, 'Question_not_understand')} 
              value={Question_not_understand} 
              // style={globalStyles.inputMulti} 
              // returnKeyType='done'
            //   keyboardType={Platform.OS == 'android' ? "number-pad" : "numeric" }
              // placeholderTextColor="gray" 
              // placeholder="Enter Things You Don't Understand"
               />
                </View>
              <Text style={globalStyles.inputLabel}>Thing Liked During The Service</Text>
              <View style={globalStyles.textAreaContainer} >
             
             <TextInput 
               style={globalStyles.textArea}
               underlineColorAndroid="transparent"
               placeholder="Enter Thing Liked During The Service"
               placeholderTextColor="gray"
               numberOfLines={10}
               multiline={true}
              onChangeText={value => onChange(value, 'Liked_things')} 
              value={Liked_things} 
              // style={globalStyles.inputMulti} 
              // returnKeyType='done'
            //   keyboardType={Platform.OS == 'android' ? "number-pad" : "numeric" }
            //   placeholderTextColor="gray" 
              // placeholder="Enter Thing Liked During The Service"
               />
              </View>
              <Text style={globalStyles.inputLabel}>Key Points</Text>
              <View style={globalStyles.textAreaContainer} >
             
             <TextInput 
               style={globalStyles.textArea}
               underlineColorAndroid="transparent"
               placeholder="Enter Key Points"
               placeholderTextColor="gray"
               numberOfLines={10}
               multiline={true}
              onChangeText={value => onChange(value, 'Key_points')} 
              value={Key_points} 
              // style={globalStyles.inputMulti} 
              // returnKeyType='done'
            //   keyboardType={Platform.OS == 'android' ? "number-pad" : "numeric" }
            //   placeholderTextColor="gray" 
              // placeholder="Enter Key Points"
               />
              </View>
            <CustomButton top={20} title="Edit Sermon" func={() => editSermon()} />
            <CustomButton top={10} title="Delete Sermon" func={() => deleteSermon()} />
          </View>
        ) : (
          <Text>Loading ...</Text>
        )
      }

    </View>
    </ScrollView>
    </KeyboardAvoidingView>
  )
}

export default EditSermonScreen;


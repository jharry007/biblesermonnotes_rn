import { StackScreenProps } from '@react-navigation/stack'
import React, { useContext, useEffect } from 'react'
import { useState } from 'react'
import { Dimensions, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import CustomButton from '../components/CustomButton'
import { DatabaseContext } from '../database/DatabaseContext'
import { UsersStackParams } from '../navigation/UsersNavigation'
import { COLORS } from '../styles/Constants'
import { globalStyles } from '../styles/globalStyles'

import {
  LineChart
} from "react-native-chart-kit";
interface Props extends StackScreenProps<UsersStackParams, 'ProfileScreen'> { }

const ProfileScreen = ({ navigation }: Props) => {
  const [name, setName] = useState<string>();
  const { sermonsAgg, getSermonDataByMonth } = useContext(DatabaseContext);
  const { user, currentTotal, initialInvs, currentPrice } = useContext(DatabaseContext)

  useEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <TouchableOpacity
          onPress={() => navigation.navigate('EditProfileScreen', {})}
          activeOpacity={0.8}
        >
          <Icon style={{ marginRight: 20 }} name="settings" color={COLORS.primary} size={24} />
        </TouchableOpacity>
      )
    })
    if (user.Name.length > 0) setName(user.Name)
    console.log("CHECK HERE", user.Name);

  })

  // const commaSepMonth = sermonsAgg.map(item => item.month).join(', ');
  // console.log(commaSepMonth);
  // const commaSepSermonCout = sermonsAgg.map(item => item.sermonCount).join(', ');
  // console.log(commaSepSermonCout);
  const line = {
    labels: sermonsAgg.map(item => (item.month).toString().substring(0, 3)),
    datasets: [
      {
        data: sermonsAgg.map(item => item.sermonCount),
        strokeWidth: 1, // optional
      },
    ],
  };

  return (
    <View style={globalStyles.viewGraphContainer}>
      <Text style={globalStyles.title}>
        Profile Screen
      </Text>
      {name ? (
        <Text style={style.subTitle}>
          Welcome, {name}
        </Text>
      ) : (
        <Text style={style.subTitle}>
          User Profile Not Set
        </Text>
      )}
      {/* <Text style={{ fontWeight: 'bold', fontSize: 20 }}>Summary</Text>
      <Text style={style.customeFont}>{'\n'}
        <Text>You currently have
          <Text style={{ fontWeight: 'bold' }}> {(currentTotal / currentPrice).toFixed(2)} </Text>
          stocks
        </Text>{'\n'}
        <Text style={{ color: 'black' }}>Your Gain/Loss is 
          <Text style={currentTotal >=  initialInvs ? { color: 'green', fontWeight: 'bold'} : { color: 'red', fontWeight: 'bold' }}>{' '}
            ${(currentTotal - initialInvs).toFixed(2)}
          </Text>
        </Text>{'\n'}
        <Text>
          Your current Capital is 
            <Text style={{fontWeight: 'bold'}}>{' '}
              ${currentTotal.toFixed(2)}
            </Text>
        </Text>{'\n'}
        <Text>And your initial Investment is
          <Text style={{ fontWeight: 'bold', color: 'grey' }}> ${initialInvs.toFixed(2)}</Text>
        </Text>
      </Text> */}
      <View >
      {sermonsAgg.length === 0 ? (
          <>
          <Text style={{color: 'grey', fontSize: 20}}>Press The Add Button To Begin</Text>
          </>
        ) : (
        
        <LineChart
          data={line}
          width={Dimensions.get('window').width} // from react-native
          height={220}
          yAxisLabel={''}
          chartConfig={{
            backgroundColor: '#e26a00',
            backgroundGradientFrom: 'green',
            backgroundGradientTo: '#ffa726',
            decimalPlaces: 1, // optional, defaults to 2dp
            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
            style: {
              borderRadius: 16
            }
          }}
          bezier
          style={{
            marginVertical: 8,
            borderRadius: 16,
          }}
        />)
      }
    
  </View>
    </View>
    
  )
}

const style = StyleSheet.create({
  subTitle: {
    marginVertical: 20,
    fontSize: 22,
    fontWeight: '600'
  },
  customeFont: {
    fontWeight: '400',
    fontSize: 18
  }
})

export default ProfileScreen

import { TabActions } from '@react-navigation/native'
import React, { useContext } from 'react'
import { Alert, Keyboard, KeyboardAvoidingView, Platform, SafeAreaView, ScrollView, StyleSheet, TextInput } from 'react-native'
import { View } from 'react-native'
import { Text } from 'react-native'
import CustomButton from '../components/CustomButton'
import { DatabaseContext } from '../database/DatabaseContext'
import { useForm } from '../hooks/useForm'
import { COLORS } from '../styles/Constants'
import { globalStyles } from '../styles/globalStyles'
import { InterstitialAd, TestIds, AdEventType} from '@react-native-firebase/admob';

const AddSermonScreent = ({navigation}:any) => {
    const { setSermonData } = useContext(DatabaseContext)
    const { Key_verses, Speaker_name, Question_not_understand, Liked_things, Key_points, Category, onChange, setFormValue } = useForm({
        Key_verses: '',
        Speaker_name: '',
        Question_not_understand: '',
        Liked_things:'',
        Key_points:'',
        Category:''
    })

    const showInterstitialAd = () => {
      // Create a new instance
      const interstitialAd = InterstitialAd.createForAdRequest('ca-app-pub-4646433692422477/4086872137');
  
      // Add event handlers
      interstitialAd.onAdEvent((type, error) => {
          if (type === AdEventType.LOADED) {
              interstitialAd.show();
          }
      });
  
      // Load a new advert
      interstitialAd.load();
  }

    const createSermonNote = () => {
        setSermonData(Key_verses, Speaker_name, Question_not_understand, Liked_things, Key_points, Category);
        const jump = TabActions.jumpTo('Home');
        navigation.dispatch(jump)
        setFormValue({ Key_verses: '',
        Speaker_name: '',
        Question_not_understand: '',
        Liked_things:'',
        Key_points:'',
        Category:''
    });
    showInterstitialAd();
      }

      const keyboardVerticalOffset = Platform.OS === 'ios' ? 40 : 0
    return (
        <SafeAreaView style={globalStyles.viewContainer}>
          <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={{flex: 1}}
      enabled={true}>
      <ScrollView>
          <Text style={globalStyles.title}>
            Add Sermon Note
          </Text>
          <View style={globalStyles.inputBox}>
            <Text style={globalStyles.inputLabel}>Key Verses</Text>
            <TextInput 
              onChangeText={value => onChange(value, 'Key_verses')} 
              value={Key_verses} 
              style={globalStyles.input} 
              placeholder="Sermon Key Verses" 
              placeholderTextColor="gray"/>
            <Text style={globalStyles.inputLabel}>Speaker Name</Text>
            <TextInput 
              onChangeText={value => onChange(value, 'Speaker_name')} 
              value={Speaker_name} 
              style={globalStyles.input} 
              returnKeyType='done'
            //   keyboardType={Platform.OS == 'android' ? "number-pad" : "numeric" }
              placeholder="Enter Speaker Name" 
              placeholderTextColor="gray"/>
               <Text style={globalStyles.inputLabel}>Category</Text>
            <TextInput 
              onChangeText={value => onChange(value, 'Category')} 
              value={Category} 
              style={globalStyles.input} 
              returnKeyType='done'
            //   keyboardType={Platform.OS == 'android' ? "number-pad" : "numeric" }
              placeholder="Enter Category" 
              placeholderTextColor="gray"/>
            <Text style={globalStyles.inputLabel}>Ask questions about things didn’t understand</Text>
               <View style={globalStyles.textAreaContainer} >
             
            <TextInput 
              style={globalStyles.textArea}
              underlineColorAndroid="transparent"
              placeholder="Enter Things You Don't Understand"
              placeholderTextColor="gray"
              numberOfLines={10}
              multiline={true}
              onChangeText={value => onChange(value, 'Question_not_understand')} 
              value={Question_not_understand} 
              // style={globalStyles.inputMulti} 
              // returnKeyType='done'
            //   keyboardType={Platform.OS == 'android' ? "number-pad" : "numeric" }
              // placeholderTextColor="gray" 
              // placeholder="Enter Things You Don't Understand"
               />
                </View>
              <Text style={globalStyles.inputLabel}>Thing Liked During The Service</Text>
              <View style={globalStyles.textAreaContainer} >
             
             <TextInput 
               style={globalStyles.textArea}
               underlineColorAndroid="transparent"
               placeholder="Enter Thing Liked During The Service"
               placeholderTextColor="gray"
               numberOfLines={10}
               multiline={true}
              onChangeText={value => onChange(value, 'Liked_things')} 
              value={Liked_things} 
              // style={globalStyles.inputMulti} 
              // returnKeyType='done'
            //   keyboardType={Platform.OS == 'android' ? "number-pad" : "numeric" }
            //   placeholderTextColor="gray" 
              // placeholder="Enter Thing Liked During The Service"
               />
              </View>
              <Text style={globalStyles.inputLabel}>Key Points</Text>
              <View style={globalStyles.textAreaContainer} >
             
             <TextInput 
               style={globalStyles.textArea}
               underlineColorAndroid="transparent"
               placeholder="Enter Key Points"
               placeholderTextColor="gray"
               numberOfLines={10}
               multiline={true}
              onChangeText={value => onChange(value, 'Key_points')} 
              value={Key_points} 
              // style={globalStyles.inputMulti} 
              // returnKeyType='done'
            //   keyboardType={Platform.OS == 'android' ? "number-pad" : "numeric" }
            //   placeholderTextColor="gray" 
              // placeholder="Enter Key Points"
               />
              </View>
            <CustomButton top={20} title="Create Sermon Note" func={() => createSermonNote() } />
          </View>
          </ScrollView>
    </KeyboardAvoidingView>
        </SafeAreaView>
        
      )
    }
    
export default AddSermonScreent
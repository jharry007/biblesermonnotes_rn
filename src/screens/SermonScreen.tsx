import { StackScreenProps } from '@react-navigation/stack';
import React, { useContext, useEffect, useState } from 'react'
import { Alert,FlatList, Image, RefreshControl, ScrollView } from 'react-native';
import { Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import SplashScreen from 'react-native-splash-screen';
import CardTile from '../components/CardTile';
import { CryptoStackParams } from '../navigation/CryptosNavigation'
import { DatabaseContext } from '../database/DatabaseContext';
import { globalStyles } from '../styles/globalStyles';
import { homeScreenStyle } from '../styles/homeStyles';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { ListItem, Avatar,Button, Badge, Overlay } from 'react-native-elements';
import Moment from 'moment';

interface Props extends StackScreenProps<CryptoStackParams, 'SermonScreen'> { };

const SermonScreen = ({ navigation, route }: Props) => {
  const { sermons, getSermonsDetailByMonth } = useContext(DatabaseContext);
  const [loading, setLoading] = useState<boolean>(false);
  const { routedId='', routedName } = route.params;
  const [isRefreshing, setIsRefreshing] = useState<boolean>(false)
  

  useEffect(() => {
    loadSermon();
  }, [routedId])

//   useEffect(() => {
//     setValues()
//   }, [singleSermon])

  const loadSermon = async () => {
      //const Id = routedId;
    setLoading(true);
    if (routedId === '') {
      Alert.alert("Id is null")
      return;
    }

    getSermonsDetailByMonth(routedId);
    setLoading(false);
  }

  const [visible, setVisible] = useState(false);

  const toggleOverlay = () => {
    console.log('toggle clicked');
    setVisible(!visible);
  };


  const refreshData = async () => {
    setIsRefreshing(true)
    await getSermonsDetailByMonth(routedId);
    //await getPrice();
    setIsRefreshing(false)
  }

  const StockList = () => {
    return (
      <View style={homeScreenStyle.listBox}>
        
        {/* <View style={homeScreenStyle.header}>
          <Text style={homeScreenStyle.headerText}>Asset</Text>
          <Text style={homeScreenStyle.headerText}>Buy Price</Text>
          <Text style={homeScreenStyle.headerText}>Current</Text>
          <Text style={homeScreenStyle.headerText}>Qty</Text>
        </View> */}
        {sermons.length === 0 ? (
          <>
          <Text style={[globalStyles.title, {color: 'grey', marginTop: 40}]}>
            You Haven't Added Any Sermon
          </Text>
          <Text style={{color: 'grey', fontSize: 30}}>Press The Add Button To Begin</Text>
          </>
        ) : (
          
          <FlatList
            showsVerticalScrollIndicator={false}
            ItemSeparatorComponent = { FlatListItemSeparator }
            numColumns={1}
            data={sermons}
            keyExtractor={(c) => String(c.Id)}
            renderItem={({ item }) => (
            
          <ListItem.Swipeable
  leftContent={
    <Button
      title="Export"
      icon={{ name: 'info', color: 'white' }}
      buttonStyle={{ minHeight: '100%', backgroundColor: 'green' }}
      onPress={() => navigation.navigate('ViewSermonScreen', {
        routedId: item.Id
      })}
    />
  }
  
  rightContent={
    <Button
      title="Edit"
      icon={{ name: 'edit', color: 'white' }}
      buttonStyle={{ minHeight: '100%', backgroundColor: 'green' }} 
      onPress={() => navigation.navigate('EditSermonScreen', {
        routedId: item.Id
      })}
    />
    
  }
>
  {/* <Icon name="My Icon" /> */}
  <ListItem.Content>
  {/* <Icon name="place" size={30} /> */}
   
    <ListItem.Title onPress={() => navigation.navigate('EditSermonScreen', {
        routedId: item.Id
      })}>{item.Key_verses}</ListItem.Title>
       <View style={globalStyles.subtitleView}>
          {/* <Image source={require('../images/rating.png')} style={globalStyles.ratingImage}/> */}
          <Badge
    status="success"
    value={(Moment(item.SermonDate).format('MM/DD/YYYY'))}
  />
          <Text style={globalStyles.ratingText}>{item.Speaker_name} </Text>
        </View>

  </ListItem.Content>
  <ListItem.Chevron />
</ListItem.Swipeable>
               
            )}
            refreshControl={
              <RefreshControl
                refreshing={isRefreshing}
                onRefresh={refreshData}
              />
            }
          />
          
        )}
        
      </View>
    )
  }

  const FlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "100%",
          backgroundColor: "#000",
        }}
      />
    );
  }

  const FlatListHeader = () => {
    return (
      <View 
        style={{
          height: 100,
          width: "97%",
          margin: 5,
          backgroundColor: "#fff",
          // border: 3,
          borderColor: "black",
          alignSelf: "center",
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 16,
          },
          shadowOpacity: 1,
          shadowRadius: 7.49
        }}
      >
        <Text style={{  textShadowColor: 'black', textShadowOffset: { width: 1, height: 3 },textShadowRadius: 10, fontSize: 40, fontWeight: '800', flex: 1, alignSelf: "center", paddingTop: 30, fontSize: 40}}>Latest articles</Text>
      </View>
    );
  }

  // const PriceTicker = () => {
  //   return (
  //     <View>
  //     {/* <Button title="Open Overlay" onPress={toggleOverlay} /> */}

  //     <Overlay style={{flex:1}} isVisible={visible} onBackdropPress={toggleOverlay}>
  //       <Text>Hello from Overlay!</Text>
  //     </Overlay>
  //   </View>
  //   )
  // }

  return (
    <View style={globalStyles.viewContainer}>
      {/* Header */}
      <View>
        <Text style={globalStyles.title}>
          {routedId}
        </Text>
      </View>
      {/* <PriceTicker /> */}
      {/* <PortfolioHeader /> */}
      <StockList />
    </View>
  )
  // return (
  //   <View>
  //     <Button title="Open Overlay" onPress={toggleOverlay} />

  //     <Overlay style={{flex:1}} isVisible={visible} onBackdropPress={toggleOverlay}>
  //       <Text>Hello from Overlay!</Text>
  //     </Overlay>
  //   </View>
  // )
  
}

export default SermonScreen

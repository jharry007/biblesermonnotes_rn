export interface Sermon {
    Id: number;
    Key_verses: string;
    Speaker_name: string;
    Question_not_understand: string;
    Liked_things:string;
    Key_points:string;
    Category:string;
    SermonDate:string;
  }
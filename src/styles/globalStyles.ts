import { StyleSheet,Dimensions } from "react-native";
import { COLORS } from "./Constants";
export const globalStyles = StyleSheet.create({
  viewContainer: {
    margin: 20,
    height: '100%',
  },
  viewGraphContainer: {
    margin: 1,
    height: '100%',
  },
  title: {
    fontSize: 35,
    fontWeight: 'bold',
  },
  inputBox: {
    marginTop: 20,
    flex: 1
  },
  inputLabel: {
    marginTop: 10,
    color: "#00af00"
  },
  input: {
    minHeight: 50,
    borderRadius: 6,
    marginTop: 5,
    borderColor: 'lightgrey',
    color: COLORS.black,
    fontSize: 16,
    borderWidth: 2,
    paddingLeft: 20
  },
  inputMulti: {
    minHeight: 50,
    borderRadius: 6,
    height:150,
    // marginTop: 5,
    borderColor: 'lightgrey',
    color: COLORS.black,
    fontSize: 16,
    borderWidth: 2,
    paddingLeft: 20
  },
  textAreaContainer: {
    borderColor: 'lightgrey',
    borderRadius: 6,
    borderWidth: 1,
    padding: 5
  },
  textArea: {
    height: 150,
    color: COLORS.black,
    fontSize: 16,
    justifyContent: "flex-start"
  },
  screen: {
    marginTop: 30,
  },
  row: {
    margin: 15,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: 2,
  },
  rowText: {
    fontSize: 18,
  },
  subtitleView: {
    flexDirection: 'row',
    paddingLeft: 10,
    paddingTop: 5
  },
  ratingImage: {
    height: 19.21,
    width: 100
  },
  ratingText: {
    paddingLeft: 10,
    color: 'grey'
  },
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: 25,
},
pdf: {
    flex:1,
    width:Dimensions.get('window').width,
    height:Dimensions.get('window').height,
}
})